package com.example.secretnotes.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secretnotes.R;
import com.example.secretnotes.model.Notes;
import com.example.secretnotes.ui.NoteUpdateActivity;
import com.example.secretnotes.view_holder.NotesListViewHolder;

import java.util.List;

public class NotesListAdapter extends RecyclerView.Adapter {
    List<Notes> notesList;

    public NotesListAdapter(List<Notes> notesList) {
        this.notesList = notesList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_view_holder, parent, false);
        NotesListViewHolder holder = new NotesListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((NotesListViewHolder) holder).textView.setText(notesList.get(position).getNotesBody());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noteUpdate = new Intent(v.getContext(), NoteUpdateActivity.class);
                noteUpdate.putExtra("note", notesList.get(position).getNotesBody());
                noteUpdate.putExtra("id", notesList.get(position).getNotesBody());

                ((Activity) v.getContext()).startActivityForResult(noteUpdate, 1);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(v.getContext())
                        .setTitle("are you sure")
                        .setMessage("Do you want to delete this note")
                        .setPositiveButton("Olrait", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Nope", null).show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.notesList.size();
    }
}