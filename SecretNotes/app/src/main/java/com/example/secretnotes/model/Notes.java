package com.example.secretnotes.model;

public class Notes {
    String id;
    String notesBody;

    public Notes(String id, String notesBody) {
        this.id = id;
        this.notesBody = notesBody;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotesBody() {
        return notesBody;
    }

    public void setNotesBody(String notesBody) {
        this.notesBody = notesBody;
    }
}
