package com.example.secretnotes.repository;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.secretnotes.model.Notes;
import com.example.secretnotes.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NotesRepository {
    public void updateNotes(String id, String note) {

        SQLiteDatabase db = MainActivity.dataHelper.getWritableDatabase();
        SQLiteStatement stmt;
        if (id != null && id.length() == 0) {
            id = String.valueOf(new Random().nextInt());
            stmt = db.compileStatement("INSERT INTO notes VALUES(?,?)");
            stmt.bindString(1, id);
            stmt.bindString(2, note);
        } else {
            stmt = db.compileStatement("UPDATE notes SET notes_body=? WHERE id = ?");
            stmt.bindString(1, note);
            stmt.bindString(2, id);
        }
        stmt.execute();
    }
    public List<Notes> selectAllNotes(){
        SQLiteDatabase db = MainActivity.dataHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM notes", null);
        List<Notes> result = new ArrayList<>();

        if (c != null){
            if (c.moveToFirst()){
                do {
                    result.add(new Notes(c.getString(0), c.getString(1)));
                }while (c.moveToNext());
            }
        }
        c.close();
        return result;
    }
}
