package com.example.secretnotes.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.secretnotes.R;
import com.example.secretnotes.adapter.NotesListAdapter;
import com.example.secretnotes.model.Notes;
import com.example.secretnotes.repository.NotesRepository;

import java.util.ArrayList;
import java.util.List;

public class NoteListActivity extends AppCompatActivity {
    List<Notes> notes = new ArrayList<>();
    NotesListAdapter notesListAdapter;
    RecyclerView recyclerView;

    NotesRepository notesRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        notes.add(new Notes("1", "blabla"));
//        notes.add(new Notes("2", "blabla"));


        setContentView(R.layout.activity_note_list);
        recyclerView = findViewById(R.id.recycleViewList);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layout);

        notesListAdapter = new NotesListAdapter(notes);
        recyclerView.setAdapter(notesListAdapter);
        notesListAdapter.notifyDataSetChanged();

        notesRepository = new NotesRepository();
        getNoteList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuAdd) {
            Intent intent = new Intent(this, NoteUpdateActivity.class);
            startActivityForResult(intent, 1);
        }
        if (id == R.id.btnQR){
            Intent intent = new Intent(this, QRActivity.class);
            startActivityForResult(intent, 1);
        }
        return super.onOptionsItemSelected(item);
    }


    private void getNoteList() {
        notes = notesRepository.selectAllNotes();
        notesListAdapter = new NotesListAdapter(notes);
        recyclerView.setAdapter(notesListAdapter);
        notesListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                getNoteList();
            }
        }
    }
}
