package com.example.secretnotes.ui;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.secretnotes.R;
import com.example.secretnotes.repository.NotesRepository;

public class NoteUpdateActivity extends AppCompatActivity {
    EditText multiLineNote;
    String idNote;
    String note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_update);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View customMenu = layoutInflater.inflate(R.layout.notes_update_menu, null);
        actionBar.setCustomView(customMenu);

        multiLineNote = findViewById(R.id.txtNoteUpdate);

        Intent intent = getIntent();
        idNote = intent.getStringExtra("id");
        note = intent.getStringExtra("note");
        multiLineNote.setText(note);

        ImageView button = findViewById(R.id.btnSave);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idNote == null) {
                    idNote = "";
                }
                NotesRepository notesRepository = new NotesRepository();
                notesRepository.updateNotes(idNote, multiLineNote.getText().toString());
                Intent replyIntent = new Intent();
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }
}
