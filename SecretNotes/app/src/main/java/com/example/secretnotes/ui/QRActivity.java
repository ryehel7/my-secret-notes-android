package com.example.secretnotes.ui;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.secretnotes.R;
import com.example.secretnotes.repository.NotesRepository;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private TextView txtQrResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Scan QR");
        mScannerView = new ZXingScannerView(this);
        mScannerView.setAutoFocus(true);
        mScannerView.setResultHandler(this);

        FrameLayout frameLayout = findViewById(R.id.frameQr);
        frameLayout.addView(mScannerView);

        txtQrResult = findViewById(R.id.txtQrResult);
        Button btnSaveQrResult = findViewById(R.id.btnSaveQrResult);
        btnSaveQrResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotesRepository notesRepository = new NotesRepository();
                notesRepository.updateNotes("", txtQrResult.getText().toString());
                Intent replyIntent = new Intent();
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }

    @Override
    public void onResume(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},100);
        }
        mScannerView.startCamera();
        super.onResume();
    }
    @Override
    public void onPause(){
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        txtQrResult.setText(rawResult.getText());

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
