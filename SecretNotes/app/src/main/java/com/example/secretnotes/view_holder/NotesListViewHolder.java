package com.example.secretnotes.view_holder;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.secretnotes.R;

public class NotesListViewHolder extends RecyclerView.ViewHolder {
    public TextView textView;

    public NotesListViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.textView);
    }
}
